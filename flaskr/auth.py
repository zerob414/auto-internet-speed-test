import functools
import re
from bson.objectid import ObjectId

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app
)
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

blueprint = Blueprint('auth', __name__, url_prefix='/auth')

@blueprint.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        password2 = request.form['password2']
        db = get_db()[current_app.config['DB_NAME']]
        error = None

        if not username:
            error = 'Username is required.'
        elif len(username) > 64:
            error = "Username must be less than 64 characters in length."
        elif not password:
            error = 'Password is required.'
        elif not is_password_valid(password):
            error = "Passwords must be at least 8 characters in length, contain lowercase and capital letters, and at least one digit."
        elif password != password2:
            error = "The passwords don't match."
        elif db.users.find_one({"username": username}) is not None:
            error = 'User {} is already registered.'.format(username)

        if error is None:
            db.users.insert_one({
                "username": username,
                "password": generate_password_hash(password, method="pbkdf2:sha256", salt_length=16)
            })
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html', username = username, password = password)

def is_password_valid(password: str):
    return len(password) >= 8 and re.search(r"[a-z]", password) is not None and re.search(r"[A-Z]", password) is not None and re.search(r"[0-9]", password) is not None

@blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()[current_app.config['DB_NAME']]
        error = False
        user = db.users.find_one({"username": username})

        if user is None:
            error = True
        elif not check_password_hash(user['password'], password):
            error = True

        if error == False:
            session.clear()
            session['user_id'] = str(user['_id'])
            return redirect(url_for('index'))
        else:
            flash("The username and password combination you supplied is not valid.")

    return render_template('auth/login.html')

@blueprint.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        db = get_db()[current_app.config['DB_NAME']]
        g.user = db.users.find_one({"_id": ObjectId(user_id)})

@blueprint.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view