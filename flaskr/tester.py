import speedtest
from pymongo import MongoClient

class Main:
    __database_client = None
    __database = None

    def get_mongodb(self):
        ''' Creates a database client and connects to the MongoDB database instance.
        If a client already exists, the existing client will be used. The
        database for the application is returned from this method. '''
        if self.__database_client is None:
            # TODO: Load connection settings from a file
            host = "localhost"
            port = 27017
            self.__database_client = MongoClient(host, port)
            self.__database = self.__database_client.internet_speed_tester
        return self.__database

    def close_mongodb(self):
        ''' Closes the MongoDB connection and sets the related fields to None. '''
        if not self.__database_client is None:
            self.__database_client.close()
            self.__database_client = None
            self.__database = None

    def test(self):
        ''' Performs the speed test and stores the results in the MongoDB database.'''
        threads = 1
        tester = speedtest.Speedtest()
        tester.get_best_server()
        tester.download(threads=threads)
        tester.upload(threads=threads)
        db = self.get_mongodb()
        test_results_collection = db.test_results
        test_results_collection.insert_one(tester.results.dict())
        self.close_mongodb()


if __name__ == "__main__":
    main = Main()
    main.test()