from pymongo import MongoClient
import click
from flask import current_app, g
from flask.cli import with_appcontext

def get_db():
    if not 'db' in g:
        g.db = MongoClient(current_app.config['DB_HOST'], current_app.config['DB_PORT'])
    return g.db

def close_db(e = None):
    db = g.pop('db', None)
    if not db is None:
        db.close()

def init_app(app):
    app.teardown_appcontext(close_db)