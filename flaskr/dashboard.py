import re, pymongo

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

blueprint = Blueprint('dashboard', __name__)

@blueprint.route('/')
@login_required
def index():
    db = get_db()[current_app.config['DB_NAME']]
    # TODO: Implement pagination
    results = db.test_results.find().sort("timestamp", pymongo.DESCENDING)
    return render_template('dashboard/index.html', results=results)

def cursor_to_html(source: pymongo.cursor.Cursor) -> str:
    html = ""
    for doc in source:
        if isinstance(doc, pymongo.cursor.Cursor):
            html += "<p class='indent'>{}</p>".format(cursor_to_html(doc))
        else:
            html += "<strong>{}:</strong> {}<br>".format(str(doc.get('name')), str(doc))
    return html

def dict_to_html(source: dict) -> str:
    html = ""
    for key, value in source.items():
        if isinstance(value, dict):
            html += "<p class='indent'>{}</p>".format(dict_to_html(value))
        else:
            html += "<strong>{}:</strong> {}<br>".format(str(key), str(value))
    return html

@blueprint.route('/runtest')
@login_required
def runtest():
    return "TODO: Implement running a test on demand."