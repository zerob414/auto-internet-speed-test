# Speed Monitor

A small project that tests internet speeds periodically and records the results. Provides a web interface for displaying recorded data.

This project is intended to run on a Raspberry Pi connected to your home network. It was developed using the following:
- Raspian Stretch, based on Debian Linux 9.12 Stretch (Desktop edition running in VirtualBox)
- MongoDB 3.2.11, installed via apt-get
- FontAwesome Free 5.13.0
- Twitter Bootstrap (linked from CDN in base.html)

## FontAwesome
Download and extract the free package from fontawesome.com. You should have the following from the FontAwesome download:
- `flaskr/static/fontawesome/webfonts/` This directory will contain several files from FontAwesome that have the extensions: `.eot`, `.svg`, `.ttf`, `.woff`, and `.woff2`.
- `flaskr/static/fontawesome/css/all.css` This is the CSS file that makes FontAwesome work.